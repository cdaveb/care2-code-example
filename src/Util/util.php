<?php
/**
 * Track any messages to be displayed on next session view
 *
 * @param string $err the error message
 * @param string $type (optional) the error type - defaults to danger
 */
function add_session_message(string $err, string $type='danger')
{
    if (empty($err)) {
        return;
    }

    if (empty($_SESSION['messageList'])) {
        $_SESSION['messageList'] = [];
    }

    $_SESSION['messageList'][] = ['content' => $err, 'type' => $type];
}

/**
 * Return the current list of session messages and reset them
 *
 * @return array
 */
function get_and_reset_session_messages(): array
{
    $messageList = $_SESSION['messageList'] ?? [];

    $_SESSION['messageList'] = [];

    return $messageList;
}

/**
 * Add a danger message to the logs and redirect to the specified page (defaults to home page), then exit
 *
 * @param string $err the error message
 * @param string $url (optional) the URL to redirect to - defaults to /index.php
 */
function error_and_redirect(string $err, string $url='')
{
    add_session_message($err, 'danger');
    header('Location: ' . (empty($url) ? '/' : $url));
    exit();
}
