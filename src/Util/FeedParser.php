<?php

namespace App\Util;

use App\Model\Feed;
use \Exception;
use \JsonException;

class FeedParser
{
    public const BASE_FEED_URL = 'https://www.thepetitionsite.com/servlets/petitions/feed.php?type=publisher&feedID=';
    public const DEFAULT_FEED_ID = 2382;
    public const VALID_PETITION_COLS = [
        'title', 'link', 'description', 'petitionID', 'signature_count', 'summary', 'stopdate'
    ];

    protected int $feed_id;
    protected ?Feed $feed = null;
    protected ?array $feed_content = null;

    /**
     * Initializes the feed parser for a specified feed ID
     *
     * @param int $feed_id the feed ID to use for obtaining the content
     */
    public function __construct(int $feed_id)
    {
        $this->feed_id = $feed_id;
    }

    /**
     * Returns the URL for the feed based on the configured feed ID
     *
     * @return string the feed URL
     */
    public function getFeedUrl(): string
    {
        return static::BASE_FEED_URL . $this->feed_id;
    }

    /**
     * Sets the feed content
     *
     * @param array $data the feed content in array format
     */
    protected function setFeedContent(array $data)
    {
        $this->feed = new Feed($data);
    }

    /**
     * Loads the content from the configured feed URL, json decodes it and stores it
     */
    protected function loadFeedContent()
    {
        // Load data from feed
        if (($feed_content = @file_get_contents($this->getFeedUrl())) === false) {
            $error = error_get_last();
            throw new Exception('Could not load feed');
        }

        // Decode JSON data from feed
        try {
            $decodedData = json_decode($feed_content, true, $depth=512, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            throw new Exception('Could not parse feed');
        }

        $this->setFeedContent($decodedData);
    }

    /**
     * Return the content from the configured feed, loading from the URL if not already cached
     *
     * @return array the contents of the feed
     */
    public function getFeed(): ?Feed
    {
        // If the feed content hasn't been loaded yet, retrieve the data
        if (empty($this->feed)) {
            $this->loadFeedContent();
        }

        return $this->feed;
    }
}
