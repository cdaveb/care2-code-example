<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Care2 Petition List</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
        <link href="/css/site.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" integrity="sha384-Xe+8cL9oJa6tN/veChSP7q+mnSPaj5Bcu9mPX5F5xIGE0DVittaqT5lorf0EI7Vk" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js" integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.6.0.slim.min.js"></script>
    </head>
    <body class="m-4">
        <h1>Care2 Petition List<?= empty($feedName) ? '' : ': ' . htmlspecialchars($feedName); ?></h1>
        <?php
        include APP_ROOT . '/src/View/display_messages.php';

        // Validate if any petitions were found
        if (empty($petitionList)):
            ?>
        <div class="alert alert-danger" role="alert">
            <p>No petitions were found in the requested feed.</p>
        </div>
        <?php
        else:
            ?>
        <form method="post" action="/export" class="mt-4" id="export_form">

            <input type="hidden" name="feedId" value="<?= $feedId; ?>">

            <div class="fixed-bottom" id="petition_export_button">
                <input type="submit" value="Export Selected Columns to CSV" class="btn btn-primary">
            </div>

            <table class="table table-striped table-bordered table-sm" id="petition_list">
                <caption>Petition List</caption>
                <thead class="sticky-top">
                    <tr>
                        <th scope="col">
                            <label for="export_title">Title</label><br>
                            <input type="checkbox" name="export[]" value="title" id="export_title" checked>
                        </th>
                        <th scope="col">
                            <label for="export_link">Link</label><br>
                            <input type="checkbox" name="export[]" value="link" id="export_link" checked>
                        </th>
                        <th scope="col">
                            <label for="export_description">Description</label><br>
                            <input type="checkbox" name="export[]" value="description" id="export_description" checked>
                        </th>
                        <th scope="col">
                            <label for="export_petition_id">Petition ID</label><br>
                            <input type="checkbox" name="export[]" value="petitionID" id="export_petition_id" checked>
                        </th>
                        <th scope="col">
                            <label for="export_signature_count">Signature Count</label><br>
                            <input type="checkbox" name="export[]" value="signature_count" id="export_signature_count" checked>
                        </th>
                        <th scope="col">
                            <label for="export_summary">Summary</label><br>
                            <input type="checkbox" name="export[]" value="summary" id="export_summary" checked>
                        </th>
                        <th>Stop Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($petitionList as $item) {
                        ?>
                    <tr class="petition-row">
                        <td scope="row" class="petition-title">
                            <?= htmlspecialchars($item->getTitle()); ?>
                        </td>
                        <td class="petition-link">
                            <?php if ($item->getLink()): ?>
                            <a
                                href="<?= str_replace('"', '%22', $item->getLink()); ?>" 
                                target="_blank" 
                                data-bs-toggle="tooltip" 
                                title="<?= htmlspecialchars($item->getLink()); ?>"
                            >
                                <?= htmlspecialchars( 
                                        (strlen($item->getLink()) > 40) ? 
                                            (substr($item->getLink(), 0, 50) . '...') : 
                                            $item->getLink() 
                                    ); ?>
                            </a>
                            <?php endif; ?>
                        </td>
                        <td class="petition-description">
                            <?= htmlspecialchars($item->getDescription()); ?>
                        </td>
                        <td class="petition-id">
                            <?= htmlspecialchars($item->getPetitionID()); ?>
                        </td>
                        <td class="petition-signature-count text-end">
                            <?= htmlspecialchars($item->getSignature_count()); ?>
                        </td>
                        <td class="petition-summary">
                            <?= htmlspecialchars($item->getSummary()); ?>
                        </td>
                        <td class="petition-stopdate">
                            <?php 
                            if ($item->getStopdate()) {
                                echo $item->getStopdate()->format('Y-m-d');
                            } ?>
                        </td>
                    </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>

        </form>

        <script>
        $(function() {
            $('[data-bs-toggle="tooltip"]').tooltip();
            $('#export_form').submit(function(e) {
                if ($('input[name="export[]"]:checked').length < 1) {
                    e.preventDefault();
                    alert('You must select at least one column to export');
                }
            })
        });
        </script>
        <?php endif; ?>
    </body>
</html>

