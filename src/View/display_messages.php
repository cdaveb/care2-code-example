<?php
$messageList = get_and_reset_session_messages();
if (!empty($messageList)) {
    foreach ($messageList as $msg) {
        ?>
    <div class="alert alert-danger" role="<?= $msg['type']; ?>">
        <?= $msg['content']; ?>
    </div>
<?php
    }
}
