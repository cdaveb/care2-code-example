<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Care2 Petition List</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
        <link href="/css/site.css" rel="stylesheet">
    </head>
    <body class="m-4">
        <h1>Care2 Petition List</h1>
        
        <p>
            The page you requested could not be found. You may wish to return to the <a href="/">home page</a>.
        </p>

    </body>
</html>


