<?php

namespace App\Controller;

class ErrorController extends BaseController
{
    /**
     * Displays the error page when a file can't be found
     */
    public function missingAction()
    {
        http_response_code(404);
        $this->renderAndExit('404');
    }
}
