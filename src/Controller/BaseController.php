<?php

namespace App\Controller;

abstract class BaseController
{
    /**
     * Render the specified view, exposing any parameters
     *
     * @param string $file the view file to render
     * @param array $params (optional) if specified, expose these parameters to the view
     */
    public function render(string $file, array $params=[])
    {
        extract($params);

        include APP_ROOT . '/src/View/' . $file .'.php';
    }

    /**
     * Render the specified view, then exit
     *
     * @param string $file the view file to render
     * @param array $params (optional) if specified, expose these parameters to the view
     */
    public function renderAndExit(string $file, array $params=[])
    {
        $this->render($file, $params);

        exit();
    }
}
