<?php

namespace App\Controller;

use App\Controller\BaseController;
use App\Util\FeedParser;
use App\Model\Petition;
use \Exception;
use \InvalidArgumentException;

class IndexController extends BaseController
{
    /**
     * Displays the feed petition list page for the requested feed
     *
     * @param int $feedId (optional) if specified, use this feed id, otherwise use the default if not passed as a parameter
     */
    public function indexAction(int $feedId=0)
    {
        // If no id was passed through the URL, check if it was passed via a param
        if (empty($id) && !empty($_GET['feedId'])) {
            if (is_numeric($_GET['feedId'])) {
                $feedId = (int) $_GET['feedId'];
            } else {
                add_session_message('Invalid feed requested. Using default feed.');
            }
        }

        // Use the default feed if no feed is specified
        if (empty($feedId)) {
            $feedId = FeedParser::DEFAULT_FEED_ID;
        }

        // Load the feed
        try {
            $parsedFeed = new FeedParser($feedId);
            $petitionList = $parsedFeed->getFeed()->getSortedPetitions('stopdate', 'DESC');
            $feedName = $parsedFeed->getFeed()->getName();
        } catch (Exception $e) {
            $petitionList = [];
        }

        $this->renderAndExit('index', [
            'petitionList' => $petitionList,
            'feedId' => $feedId,
            'feedName' => $feedName ?? '',
        ]);
    }

    /**
     * Parses the requested feed and exports the specified columns from it in CSV format
     */
    public function exportAction()
    {
        // Validate form input
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
            error_and_redirect('Invalid form request');
        }

        // If no valid feed id was specified, redirect the user to the petition form
        $feedId = (int) ($_POST['feedId'] ?? 0);
        if (empty($feedId)) {
            error_and_redirect('No feed specified to export');
        }

        // Load the feed and redirect the user if any errors were found
        try {
            $parsedFeed = new FeedParser($feedId);
        } catch (Exception $e) {
            error_and_redirect($e->getMessage());
        }

        // Validate the export columns
        $exportCols = $_POST['export'] ?? [];

        // If no export columns were specified, redirect the user to the petition form
        if (empty($exportCols) || !is_array($exportCols)) {
            error_and_redirect('No columns requested to export');
        }

        try {
            foreach ($exportCols as $tempCol) {
                if (!in_array($tempCol, Petition::VALID_PETITION_COLS)) {
                    throw new InvalidArgumentException("Invalid export column {$tempCol} specified");
                }
            }
        } catch (Exception $e) {
            error_and_redirect($e->getMessage());
        }

        // Get the petition list
        $petitionList = $parsedFeed->getFeed()->getSortedPetitions('stopdate', 'DESC');

        // If there are no petitions, notify the user instead of exporting a blank file
        if (empty($petitionList)) {
            error_and_redirect('No petitions found to export');
        }

        // Initialize CSV output
        $out = fopen('php://output', 'w');
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment;filename=petition_list.csv');

        // Header line
        fputcsv($out, $exportCols);

        // Export the petitions as CSV
        foreach ($petitionList as $item) {
            $tempRow = [];
            foreach ($exportCols as $k) {
                $method = 'get' . ucfirst($k);
                $tempRow[$k] = $item->$method() ?? '';
            }
            fputcsv($out, $tempRow);
        }

        fclose($out);
        exit();
    }
}
