<?php

namespace App\Model;

use \DateTime;
use \InvalidArgumentException;

class Petition
{
    protected ?int $petitionID = null;
    protected string $title = '';
    protected ?string $link = null;
    protected string $description = '';
    protected int $signature_count = 0;
    protected string $summary = '';
    protected ?DateTime $stopdate = null;

    public const VALID_PETITION_COLS = [
        'title', 'link', 'description', 'petitionID', 'signature_count', 'summary', 'stopdate'
    ];

    /**
     * Creates a feed using the specified feed data
     *
     * @param array $data the feed data to load
     */
    public function __construct(array $data)
    {
        foreach (static::VALID_PETITION_COLS as $col) {
            $method = 'set' . ucfirst($col);
            if (isset($data[$col])) {
                $this->$method($data[$col]);
            }
        }
    }

    /**
     * Set petition ID
     *
     * @param string|int $val the value
     */
    public function setPetitionID($val)
    {
        if (!is_numeric($val)) {
            throw new InvalidArgumentException('Invalid petition ID specified');
        }
        $this->petitionID = (int) $val;
    }

    /**
     * Get petition ID
     *
     * @return int the petition ID
     */
    public function getPetitionID(): ?int
    {
        return $this->petitionID;
    }

    /**
     * Set title
     *
     * @param string $val the title
     */
    public function setTitle(string $val)
    {
        $this->title = $val;
    }

    /**
     * Get petition title
     *
     * @return string the title
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Set link
     *
     * @param string $val the link
     */
    public function setLink(string $val)
    {
        $this->link = filter_var($val, FILTER_SANITIZE_URL);
    }

    /**
     * Get petition link
     *
     * @return string the link
     */
    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * Set description
     *
     * @param string $val the description
     */
    public function setDescription(string $val)
    {
        $this->description = $val;
    }

    /**
     * Get petition description
     *
     * @return string the description
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * Set signature count
     *
     * @param string|int $val the signature count
     */
    public function setSignature_count(int $val)
    {
        $this->signature_count = (int) $val;
    }

    /**
     * Get petition signature count
     *
     * @return string the signature count
     */
    public function getSignature_count(): int
    {
        return $this->signature_count;
    }

    /**
     * Set summary
     *
     * @param string $val the summary
     */
    public function setSummary(string $val)
    {
        $this->summary = $val;
    }

    /**
     * Get petition summary
     *
     * @return string the summary
     */
    public function getSummary(): string
    {
        return $this->summary;
    }

    /**
     * Set stop date
     *
     * @param string $val the stop date
     */
    public function setStopdate(string $val)
    {
        $this->stopdate = new DateTime($val);
    }

    /**
     * Get petition stop date
     *
     * @return string the stop date
     */
    public function getStopdate(): ?DateTime
    {
        return $this->stopdate;
    }
}
