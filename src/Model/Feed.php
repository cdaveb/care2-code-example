<?php

namespace App\Model;

use \InvalidArgumentException;

class Feed
{
    protected ?int $id = null;
    protected string $name = '';
    protected ?array $petitions = null;

    /**
     * Creates a feed using the specified feed data
     *
     * @param array $data the feed data to load
     */
    public function __construct(array $data)
    {
        if (isset($data['_feed']['feedID'])) {
            $this->setId($data['_feed']['feedID']);
        }
        if (isset($data['_feed']['name'])) {
            $this->setName($data['_feed']['name']);
        }
        if (isset($data['petitions'])) {
            $this->setPetitions($data['petitions']);
        }
    }

    /**
     * Set feed ID
     *
     * @param string|int $val the value
     */
    public function setId($val)
    {
        if (!is_numeric($val)) {
            throw new InvalidArgumentException('Invalid feed ID specified');
        }
        $this->id = (int) $val;
    }

    /**
     * Get feed ID
     *
     * @return int the feed ID
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set feed name
     *
     * @param string $val the value
     */
    public function setName(string $val)
    {
        $this->name = $val;
    }

    /**
     * Get feed name
     *
     * @return string the feed name
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set feed petitions
     *
     * @param array $val the value
     */
    public function setPetitions(array $val)
    {
        $petitionList = [];
        if (!empty($val)) {
            foreach ($val as $petitionData) {
                $petitionList[] = new Petition($petitionData);
            }
        }

        $this->petitions = $petitionList;
    }

    /**
     * Get feed petitions
     *
     * @return array the petition list
     */
    public function getPetitions(): ?array
    {
        return $this->petitions;
    }

    /**
     * Get petitions sorted in the requested order
     *
     * @param string $sort (optional) if specified, sort by the specified column
     * @param string $sortDir (optional) if specified, sort by the specified direction (defaults to ascending)
     *
     * @return array the list of petitions sorted by the specified parameters
     */
    public function getSortedPetitions(string $sort='', string $sortDir='ASC'): array
    {
        $petitions = $this->getPetitions();

        if (empty($petitions)) {
            return [];
        }

        // If sorting was requested
        if (!empty($sort)) {
            // Verify specified sort is valid
            if (!in_array($sort, Petition::VALID_PETITION_COLS)) {
                throw new InvalidArgumentException('Invalid sort key specified');
            }

            // Verify sort direction is valid
            if (!in_array($sortDir, ['DESC', 'ASC'])) {
                throw new InvalidArgumentException('Invalid sort direction specified');
            }

            $sortMethod = 'get' . ucfirst($sort);

            usort($petitions, function ($a, $b) use ($sortMethod, $sortDir) {
                return ($sortDir === 'ASC') ? $a->$sortMethod() <=> $b->$sortMethod() : $b->$sortMethod() <=> $a->$sortMethod();
            });
        }

        return $petitions;
    }
}
