<?php

require_once '../config/config.php';
require_once APP_ROOT . '/vendor/autoload.php';
require_once APP_ROOT . '/src/Util/util.php';

use App\Controller\ErrorController;
use NoahBuscher\Macaw\Macaw;

session_start();

// Handle any routing required
Macaw::get('/', '\App\Controller\IndexController@indexAction');
Macaw::get('/feedId/(:num)', '\App\Controller\IndexController@indexAction');
Macaw::any('/export', '\App\Controller\IndexController@exportAction');

// Handle missing page
Macaw::error(function () {
    $controller = new ErrorController();
    $controller->missingAction();
});

Macaw::dispatch();

exit();
