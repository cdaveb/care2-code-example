# Care2 Coding Example Project

This project is designed to load a specific feed from Care2 and display a list of petitions. You can then export the selected columns from the petition list in CSV format.

## Install

[Docker](https://www.docker.com/) is required to run this project. To install, you will need to run:

```
$ docker-compose up -d
```

## Usage

The app can be accessed at:

[http://localhost:8080](http://localhost:8080)

Optionally you can specify a feedId in the URL or as a parameter to load a specific feed. The feed defaults to ID 2382 if not specified.

For example:

```
http://localhost:8080/feedId/2373

http://localhost:8080/?feedId=2373
```

## Tests

To run the tests for the project, you can run:

```
$ docker exec -it care2_phpunit phpunit
```
