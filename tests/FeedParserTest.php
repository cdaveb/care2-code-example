<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use App\Util\FeedParser;
use App\Model\Feed;

final class FeedParserTest extends TestCase
{
    
    public function testInvalidParserIdFormat()
    {
        $feedId = 'zzz';
        $this->expectException(TypeError::class);
        $feed = new FeedParser($feedId);
    }

    public function testInvalidParserId()
    {
        $feedId = 99999999;
        $feed = new FeedParser($feedId);

        // Verify feed returned an object
        $this->assertIsObject($feed);
        
        // Verify that invalid json raises an exception
        $this->expectException(Exception::class);
        $feedObj = $feed->getFeed();
    }

    public function testValidParser()
    {
        $feedId = FeedParser::DEFAULT_FEED_ID;
        $feed = new FeedParser($feedId);
        
        // Verify feed returned an object
        $this->assertIsObject($feed);
        
        // Verify the feed URL is as expected
        $this->assertSame($feed->getFeedURL(), FeedParser::BASE_FEED_URL . $feedId);
        
        // Verify the feed data returns an object
        $feedObj = $feed->getFeed();
        $this->assertIsObject($feedObj);
        $this->assertInstanceOf(Feed::class, $feedObj);
        
        // Verify feed had valid petitions
        $petitionList = $feedObj->getSortedPetitions('stopdate', 'DESC');
        $this->assertIsArray($petitionList);
        
        // Verify sorting was correct
        $previousItem = null;
        foreach ($petitionList as $item) {
            if (isset($previousItem)) {
                $this->assertGreaterThanOrEqual($item->getStopdate(), $previousItem->getStopdate());
            }
            $previousItem = $item;
        }
    }
}
