<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use App\Model\Petition;

final class PetitionTest extends TestCase
{
    
    /**
     * Verify a petition can be created with valid data
     */
    public function testCreatePetition()
    {
        $petitionParams = [
            'title' => 'My Petition',
            'link' => 'http://www.google.com/',
            'description' => 'Testing petition',
            'petitionID' => '22',
            'signature_count' => '322',
            'summary' => '<p>Testing</p>',
            'stopdate' => '2023-01-03',
        ];

        $petition = new Petition($petitionParams);

        $this->assertIsObject($petition);

        // Verify the petition id is converted to an integer
        $this->assertIsInt($petition->getPetitionID());

        // Verify stop date is converted to date time format
        $this->assertIsObject($petition->getStopdate());
        $this->assertInstanceOf(DateTime::class, $petition->getStopdate());

        // Verify the signature count is converted to an integer
        $this->assertIsInt($petition->getSignature_count());

        // Verify that the link wasn't changed as a result of the invalid char filter
        $this->assertSame($petitionParams['link'], $petition->getLink());
    }
    
    /**
     * Verify that the sanitize link function strips problem characters from URLs
     */
    public function testCreatePetitionInvalidLink()
    {
        $petition = new Petition([
            'link' => 'http://www.example.com/中',
        ]);
        
        $this->assertIsObject($petition);
        
        // Verify that any unsafe characters are removed from the link
        $this->assertSame('http://www.example.com/', $petition->getLink());
    }
    
    /**
     * Verify that a petition ID that isn't numeric is detected
     */
    public function testCreatePetitionInvalidPetitionId()
    {
        $this->expectException(InvalidArgumentException::class);

        $petition = new Petition([
            'petitionID' => 'a',
        ]);
    }
    
    /**
     * Verify that an exception is thrown when an invalid stop date is specified
     */
    public function testCreatePetitionInvalidStopDate()
    {
        $this->expectException(Exception::class);

        $petition = new Petition([
            'stopdate' => 'abcd',
        ]);
    }
}
