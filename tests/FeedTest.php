<?php declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use App\Model\Feed;
use App\Model\Petition;

final class FeedTest extends TestCase
{
    
    /**
     * Verify a feed can be created with valid data
     */
    public function testCreateFeed()
    {
        $feedParams = [
            '_feed' => [
                'feedID' => 23,
                'name' => 'My Feed',
            ],
            'petitions' => [
                [
                    'petitionID' => 32,
                    'title' => 'My Petition',
                    'stopdate' => '2024-01-05',
                ],
                [
                    'petitionID' => 26,
                    'title' => 'My Petition 2',
                    'stopdate' => '2025-02-04',
                ],
                [
                    'petitionID' => 26,
                    'title' => 'My Petition 2',
                    'descripton' => 'My petition description',
                ],
            ]
        ];

        $petition = new Feed($feedParams);

        $this->assertIsObject($petition);

        // Verify the feed id is converted to an integer
        $this->assertIsInt($petition->getId());

        // Verify the petitions are in valid form
        $petitionList = $petition->getPetitions();
        $this->assertIsArray($petitionList);
        $this->assertInstanceOf(Petition::class, $petitionList[0]);
    }
    
    /**
     * Verify the sort functionality works as expected
     */
    public function testPetitionListSorting()
    {
        $feedParams = [
            '_feed' => [
                'feedID' => 23,
                'name' => 'My Feed',
            ],
            'petitions' => [
                [
                    'petitionID' => 32,
                    'title' => 'My Petition',
                    'stopdate' => '2024-01-05',
                ],
                [
                    'petitionID' => 26,
                    'title' => 'My Petition 2',
                    'stopdate' => '2025-02-04',
                ],
                [
                    'petitionID' => 26,
                    'title' => 'My Petition 2',
                    'descripton' => 'My petition description',
                ],
                [
                    'petitionID' => 29,
                    'descripton' => 'Another petition description',
                ],
            ]
        ];

        $petition = new Feed($feedParams);

        $this->assertIsObject($petition);

        // Validate sort by stop date
        $petitionList = $petition->getSortedPetitions('stopdate', 'DESC');
        $this->assertIsArray($petitionList);
        
        $previousItem = null;
        foreach ($petitionList as $item) {
            if (isset($previousItem)) {
                $this->assertGreaterThanOrEqual($item->getStopdate(), $previousItem->getStopdate());
            }
            $previousItem = $item;
        }

        // Validate sort by petition ID
        $petitionList = $petition->getSortedPetitions('petitionID', 'ASC');
        $this->assertIsArray($petitionList);
        
        $previousItem = null;
        foreach ($petitionList as $item) {
            if (isset($previousItem)) {
                $this->assertGreaterThanOrEqual($previousItem->getPetitionID(), $item->getPetitionID());
            }
            $previousItem = $item;
        }

        // Validate sort by title
        $petitionList = $petition->getSortedPetitions('title', 'ASC');
        $this->assertIsArray($petitionList);
        
        $previousItem = null;
        foreach ($petitionList as $item) {
            if (isset($previousItem)) {
                $this->assertGreaterThanOrEqual($previousItem->getTitle(), $item->getTitle());
            }
            $previousItem = $item;
        }
    }
}
